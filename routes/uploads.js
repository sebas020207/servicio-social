const { Router } = require("express");
const { check } = require("express-validator");
const {
  uploadfiles,
  actualizarimgen,
  eliminarimg,
 
} = require("../controllers/uploads");
const { coleccionesPermitidas } = require("../helpers/db-validators");
const { validarCampos } = require("../middlewares/validar-campos");
const { validarJWT } = require("../middlewares/validarjwt");

const router = Router();

router.post("/files", [validarJWT, validarCampos], uploadfiles);

router.put(
  "/files/:coleccion/:id",
  [
    validarJWT,
    check("id", "El id debe ser de mongo").isMongoId(),
    check("coleccion").custom((c) =>
      coleccionesPermitidas(c, ['usuarios', 'cursos'])
    ),
    validarCampos
  ],
  actualizarimgen
);

router.delete(
  "/files/:coleccion/:id",
  [
    validarJWT,
    check("id", "El id debe ser de mongo").isMongoId(),
    check("coleccion").custom((c) =>
      coleccionesPermitidas(c, ["usuarios", "cursos"])
    ),
    validarCampos,
  ],
  eliminarimg
);

module.exports = router;

const { Router } = require("express");
const { check } = require("express-validator");

const { validarCampos } = require("../middlewares/validar-campos");
const {
  esRoleValido,
  emailExiste,
  existeUsuarioPorId,
  esFacultadvalida,
} = require("../helpers/db-validators");

const {
  usuariosGet,
  usuariosPut,
  usuariosPost,
  usuariosDelete,
} = require("../controllers/usuarios");
const { validarJWT } = require("../middlewares/validarjwt");
const { esAdminrole } = require("../middlewares/esadminrole");

const router = Router();

router.get("/", [validarJWT, esAdminrole], usuariosGet);

router.put(
  "/:id",
  [
    validarJWT,
    check("password").not().isEmpty(),
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    check("nombre").not().isEmpty(),
    check("rol").custom(esRoleValido),
    check("correo", "El correo no es válido").isEmail(),
    validarCampos,
  ],
  usuariosPut
);

router.post(
  "/",
  [
    validarJWT,
    esAdminrole,
    check("nombre", "El nombre es obligatorio").not().isEmpty(),
    check("correo", "El correo no es válido").isEmail(),
    check("correo").custom(emailExiste),
    check("rol").custom(esRoleValido),
    check("facultad",  "No es un ID válido, para facultad").isMongoId(),
    check("facultad").custom(esFacultadvalida),
    validarCampos,
  ],
  usuariosPost
);

router.delete(
  "/:id",
  [
    validarJWT,
    esAdminrole,
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    validarCampos,
  ],
  usuariosDelete
);

module.exports = router;

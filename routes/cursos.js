const { Router } = require("express");
const { check } = require("express-validator");

const { validarCampos } = require("../middlewares/validar-campos");
const {
  existeCursoPorId,
  registroExiste,
  esFacultadvalida,
} = require("../helpers/db-validators");

const {
  cursosGet,
  cursosPut,
  cursosPost,
  cursosDelete,
  cursosPatch,
  cursosPublish,
  postfromexcel,
} = require("../controllers/cursos");
const { validarJWT } = require("../middlewares/validarjwt");
const { emptyfile, isexcel } = require("../middlewares/checkfiles");

const router = Router();

router.get("/", [validarJWT, validarCampos], cursosGet);

router.put(
  "/:id",
  [
    validarJWT,
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeCursoPorId),
    check("facultad", "La facultad es obligatoria").not().isEmpty(),
    check("facultad", "La facultad no es un id").isMongoId(),
    check("facultad").custom(esFacultadvalida),
    check("fecha_inicio", "La fecha de inicio es obligatoria").not().isEmpty(),
    check("fecha_inicio").isDate(),
    check("fecha_final", "La fecha final es obligatoria").not().isEmpty(),
    check("fecha_final").isDate(),
    check("telefono", "El telefono es un numero").isNumeric(),
    //check("extension", "La extensión es un numero").isNumeric(),
    check("precio", "El precio es un numero").isNumeric(),
    check("correo", "El correo no es válido").isEmail(),
    //emptyfile,
    validarCampos,
  ],
  cursosPut
);

router.post(
  "/",
  [
    validarJWT,
    check("titulo", "El titulo es obligatorio").not().isEmpty(),
    //check("titulo").custom(tituloExiste),//*TITULO YA NO ES UNICO SOLO REGISTRO INSTITUCIONAL
    check("facultad", "La facultad es obligatoria").not().isEmpty(),
    check("facultad", "La facultad no es un id valido").isMongoId(),
    check("facultad").custom(esFacultadvalida),
    check("registro_institucional", "El registro_institucional es obligatorio").not().isEmpty(),
    check("registro_institucional").custom(registroExiste),
    check("fecha_inicio", "La fecha de inicio es obligatoria").not().isEmpty(),
    check("fecha_inicio").isDate(),
    check("fecha_final", "La fecha final es obligatoria").not().isEmpty(),
    check("fecha_final").isDate(),
    check("contacto", "El contacto es obligatorio").not().isEmpty(),
    check("telefono", "El telefono es obligatorio").not().isEmpty(),
    check("telefono", "El telefono es un numero").isNumeric(),
    check("correo", "El correo es obligatorio").not().isEmpty(),
    check("correo", "El correo no es válido").isEmail(),
    //emptyfile,//*LA IMAGEN DEL CURSO YA NO ES OBLIGATORIA
    validarCampos,
  ],
  cursosPost
);

router.delete(
  "/:id",
  [
    validarJWT,
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeCursoPorId),
    validarCampos,
  ],
  cursosDelete
);

router.delete(
  "/publicar/:id",
  [
    validarJWT,
    check("id", "No es un ID válido").isMongoId(),
    check("id").custom(existeCursoPorId),
    check("publicado", "Falta booleano").not().isEmpty(),
    check("publicado", "Debe ser un valor booleano").isBoolean(),
    validarCampos,
  ],
  cursosPublish
);

router.patch("/", cursosPatch);

 router.post(
   "/excel",
   [
     validarJWT,
     emptyfile,
     isexcel,
     check("socketid", "Falta el ID del socket").not().isEmpty(),
     validarCampos,
   ],
   postfromexcel
 ); 


module.exports = router;

const { Router } = require("express");
const { check } = require("express-validator");
const {
  login,
  refresh,
  my_info,
  forgot_password,
  createnewpassword,
  captcha,
  changepassword,
} = require("../controllers/auth");
const { emailnoExiste } = require("../helpers/db-validators");
const { validarCampos } = require("../middlewares/validar-campos");
const { validarJWT } = require("../middlewares/validarjwt");

const router = Router();

router.post(
  "/login",
  [
    check("correo", "Verifique el correo").isEmail(),
    check("password", "La contraseña es obligatoria").not().isEmpty(),
    validarCampos,
  ],
  login
);

router.post(
  "/refresh",
  [
    check("refresh", "Token refresh es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  refresh
);

router.get("/my_info", [validarJWT, validarCampos], my_info);

router.put(
  "/change_password",
  [
    validarJWT,
    check("new_password", "La contraseña nueva es obligatoria").not().isEmpty(),
    check(
      "new_password",
      "La contraseña debe ser alfanumerica"
    ).isAlphanumeric(), //alfanumerica
    check(
      "new_password",
      "La contraseña debe ser minimo de 8 caracteres maximo 12"
    ).isLength({ min: 8, max: 12 }),
    check("current_password", "La contraseña actual es obligatoria").not().isEmpty(),
    validarCampos,
  ],
  changepassword
);

router.put(
  "/forgot_password",
  [
    check(
      "correo",
      "El correo proporcionado, no es un correo! Verifiquelo"
    ).isEmail(),
    check("correo").custom(emailnoExiste),
    validarCampos,
  ],
  forgot_password
);

router.put(
  "/new_password",
  [
    validarJWT,
    check("password", "La contraseña nueva es obligatoria").not().isEmpty(),
    check("password", "La contraseña debe ser alfanumerica").isAlphanumeric(), //alfanumerica
    check(
      "password",
      "La contraseña debe ser minimo de 8 caracteres maximo 12"
    ).isLength({ min: 8, max: 12 }),
    validarCampos,
  ],
  createnewpassword
);

router.post("/captcha", captcha);

/* router.post(
  "/google",
  [
    check("id_token", "Token de google es necesario").not().isEmpty(),
    validarCampos,
  ],
  googlesignin
); */

module.exports = router;

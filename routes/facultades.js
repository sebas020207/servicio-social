const { Router, request } = require("express");
const { check } = require("express-validator");
const { esAdminrole } = require("../middlewares/esadminrole");
const { validarCampos } = require("../middlewares/validar-campos");
const { validarJWT } = require("../middlewares/validarjwt");
const Facultad = require("../models/facultad");

const router = Router();

router.get("/", [validarJWT, esAdminrole, validarCampos], async (req, res) => {
  const nombrefac = req.query.facultad;
  //que el nombre de la facultad no vaya vacio
  if (nombrefac !== "") {
    const regEx = new RegExp(nombrefac, "i");
    //que exista la fac con ese name especificado
    const facultades = await Facultad.find({ facultad: regEx });
      return res.json({
        items: facultades,
      });
    
  }
  const [total, facultades] = await Promise.all([
    Facultad.countDocuments(),
    Facultad.find(),
  ]);

  res.json({
    total,
    items: facultades,
  });
});

module.exports = router;

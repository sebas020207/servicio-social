const { Router } = require("express");
const {
 publicursosget
} = require("../../controllers/public_controllers/cursos");

const router = Router();

router.get("/", publicursosget);

module.exports = router;
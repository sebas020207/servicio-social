const { response, request } = require("express");
const jwt = require("jsonwebtoken");
const { generarJWT } = require("../helpers/generar-jwt");
const usuario = require("../models/usuario");

const validarJWT = async(req= request, res = response, next) => {
  const token = req.header("token");
  if (!token) {
    return res.status(401).json({
      msg: "no hay token en la peticion",
    });
  }
  try {
    const { uid } = jwt.verify(token, process.env.SECRETPRIVKEY);
    req.uid = uid;
    const user= await usuario.findById(uid);

      if (!user) {
        return res.status(401).json({
          msg: "Usuario no existe en la BD",
        });
      }

    //verificar si el uid tiene edo en true
    if(!user.estado){
         return res.status(401).json({
           msg: "token no valido usuario con estado false",
         });
    }
    req.usuario=user;

    next();
  } catch (error) {
    return res.status(401).json({
      msg: "token no valido",
    });
  }
};

const validandcreatenewtoken = async (req = request, res = response) => {
   const { refresh } = req.body;
   try{
    const user = jwt.verify(refresh, process.env.REFRESHPRIVKEY);
    const {uid} = user;
    const token = await generarJWT(uid);
    return res.json({token});
   }catch (error){
    return res.status(401).json({
      msg: "token invalido"
    });
   }
};

module.exports = {
  validarJWT,
  validandcreatenewtoken,
};

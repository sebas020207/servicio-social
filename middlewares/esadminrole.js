const { response } = require("express")



const esAdminrole=(req, res= response, next)=>{


  
 if (!(req.usuario)) {
   return res.status(500).json({
     msg: "se quiere validar el role sin validar el token primero"
   });
 }

   const { rol ,nombre } = req.usuario;


    if (rol !== "ADMIN_ROLE" && rol !== "SUPER_ADMIN") {
      return res.status(401).json({
        msg: "NO TIENES ACCESO A ESTA FUNCION SORRY BRO",
      });
    }

    next();
}

module.exports={
    esAdminrole,
    
}
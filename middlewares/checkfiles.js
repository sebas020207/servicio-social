const { request, response } = require("express");


const emptyfile =(req=request, res= response, next)=>{
    if (
      !req.files ||
      Object.keys(req.files).length === 0 ||
      !req.files.archivo
    ) {
      res.status(400).json({ msg: "Cargue el archivo por favor" });
      return;
    }
    next();

}

const isexcel = (req = request, res = response, next) => {
 const extensionesvalidas = ["xlsx", "csv", "xls"];
  const {  name} = req.files.archivo;
  const nombrecortado = name.split(".");
  const extension = nombrecortado[nombrecortado.length - 1];
  const incluida = extensionesvalidas.includes(extension);
  if (!incluida) {
     res.status(400).json({ msg: "Solo extensiones permitidas :"+extensionesvalidas });
     return;
  }
  next();
};

module.exports = { emptyfile, isexcel };
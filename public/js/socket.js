console.log("holamundo")

const socket = io("https://api-nodefer.herokuapp.com/"); // poner url https://api-nodefer.herokuapp.com/ para el front

socket.on("enviar-mensaje", (message) => {
  console.log(message);
});

socket.on("progress", (status) => {
  console.log(status);
});

socket.on("connect", () => {
  console.log(socket.id); // "G5p5..."
});


const path = require("path");
const { v4: uuidv4 } = require("uuid");

const subirarchivo = (files, extensionesvalidas = ['zip', 'rar'], carpeta='') => {
  return new Promise((resolve, reject) => {
    const { zip } = files;
    const nombrecortado = zip.name.split(".");
    const extension = nombrecortado[nombrecortado.length - 1];
    //validar extensiones
    if (!extensionesvalidas.includes(extension)) {
      return reject(`extension no valida solo ${extensionesvalidas} `);
    }
    const nombretemp = uuidv4() + "." + extension;
    const uploadPath = path.join(__dirname, "../uploads/",carpeta, nombretemp);

    zip.mv(uploadPath, function (err) {
      if (err) {
        return reject(err);
      }

      resolve(uploadPath);
    });
  });
};

module.exports={
    subirarchivo
}
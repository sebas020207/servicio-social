const jwt = require("jsonwebtoken");

const generarJWT = (uid = '') => {
  return new Promise((resolve, reject) => {
    const payload = { uid };
    jwt.sign(
      payload,
      process.env.SECRETPRIVKEY,
      { expiresIn: "4h" },
      (err, token) => {
        if (err) {
          console.log(err);
          reject("no se pudo generar el jwt");
        } else {
          resolve(token);
        }
      }
    )
  })
}

const generarrefreshJWT = (uid = "") => {
  return new Promise((resolve, reject) => {
    const payload = { uid };
    jwt.sign(
      payload,
      process.env.REFRESHPRIVKEY,
      (err, token) => {
        if (err) {
          console.log(err);
          reject("no se pudo generar el jwt");
        } else {
          resolve(token);
        }
      }
    );
  });
};

module.exports = {
  generarJWT,
  generarrefreshJWT
};

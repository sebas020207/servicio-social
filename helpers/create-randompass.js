const { Command } = require("commander");

const alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
const integers = "0123456789";
const exCharacters = "!@#$%^&*_-=+";

const crearcontraseña =() =>{
const program = new Command();
program
  .version("1.0.0")
  .description("simple random secure password generator")
  .option("-l , --length <number>", "length of password", "8")
  .option("-nn , --no-numbers", "password to not include numbers")
  .option("-ns , --no-symbols", "password to not include symbols")
  .parse();
  const { length, numbers, symbols } = program.opts();
  return createPassword(length, numbers, symbols);

}

const createPassword = (length, hasNumbers, hasSymbols) => {
  let chars = alpha;
  if (hasNumbers) {
    chars += integers;
  }
  if (hasSymbols) {
    chars += exCharacters;
  }
  return generatePassword(length, chars);
};

const generatePassword = (length, chars) => {
  let password = "";
  for (let i = 0; i < length; i++) {
    password += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return password;
};

module.exports={crearcontraseña};
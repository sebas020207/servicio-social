const Role = require('../models/role');
const Facultad = require("../models/facultad");
const Usuario = require('../models/usuario');
const Curso = require('../models/curso');

const esRoleValido = async(rol = '') => {

    const existeRol = await Role.findOne({ rol });
    if ( !existeRol ) {
        throw new Error(`El rol ${ rol } no está registrado en la BD`);
    }
}

const esFacultadvalida = async (facultad= "") => { //comprueba la existencia de una facultad por id
  const existeFac = await Facultad.findById(facultad );
  if (!existeFac) {
    throw new Error(`La facultad con id ${facultad} no está registrada en la BD`);
  }
};

const existefacultad = async (facultad = "") =>{ // comprueba la existencia de una facultad por nombre
  const existeFac = await Facultad.findOne({ facultad:facultad });
  if (!existeFac) {
    throw new Error(`La facultad ${facultad} no está registrado en la BD`);
  }
}

const emailExiste = async( correo = '' ) => {

    // Verificar si el correo existe
    const existeEmail = await Usuario.findOne({ correo });
    if ( existeEmail ) {
        throw new Error(`El correo: ${ correo }, ya está registrado`);
    }
}

const emailnoExiste = async (correo = "") => {
  // Verificar si el correo existe
  const existeEmail = await Usuario.findOne({ correo });
  if (!existeEmail) {
    throw new Error(`El correo: ${correo}, no esta registrado`);
  }
};

const existeUsuarioPorId = async( id ) => {

    const existeUsuario = await Usuario.findById(id);
    if ( !existeUsuario ) {
        throw new Error(`El id no existe ${ id }`);
    }
}

const existeCursoPorId = async (id) => {
  const existeCurso = await Curso.findById(id);
  if (!existeCurso) {
    throw new Error(`El id no existe ${id}`);
  }
};

const coleccionesPermitidas = (coleccion='', colecciones=[]) =>{
    const incluida= colecciones.includes(coleccion);
      if (!incluida) {
        throw new Error(`La coleccion no ${coleccion} está permitida `);
      }
      return true;
}

const tituloExiste = async (titulo = "") => {
  const existetitulo = await Curso.findOne({ titulo });
  if (existetitulo) {
    throw new Error(`El titulo: ${titulo}, ya está registrado`);
  }
};

const registroExiste = async (registro_institucional = "") => {
  // Verificar si el correo existe
  const existereg = await Curso.findOne({ registro_institucional });
  if (existereg) {
    throw new Error(`El registro: ${registro_institucional}, ya está registrado`);
  }
};




module.exports = {
  esRoleValido,
  emailExiste,
  existeUsuarioPorId,
  coleccionesPermitidas,
  tituloExiste,
  existeCursoPorId,
  registroExiste,
  esFacultadvalida,
  emailnoExiste,
  existefacultad,
};


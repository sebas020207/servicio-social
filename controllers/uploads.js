const { response } = require("express");
const { google } = require("googleapis");
const fs = require("fs");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const Usuario = require("../models/usuario");
const Curso = require("../models/curso");
const decompress = require("decompress");

const extensionesvalidasimg = ["jpg", "png", "jpeg"];

const uploadfiles = async (req, res = response) => {
  const { tempFilePath, name, mimetype } = req.files.archivo;
  const nombrecortado = name.split(".");
  const extension = nombrecortado[nombrecortado.length - 1];
  const incluida = extensionesvalidasimg.includes(extension);
  if (!incluida) {
    throw new Error(
      `La extension no ${extension} está permitida, extensiones validas ${extensionesvalidasimg} `
    );
  }
  const nombretemp = uuidv4() + "." + extension;
  const keyfilepath = path.join(
    __dirname,
    "../Auth/rest-api-353417-32e9555ddd01.json"
  );
  const scopes = ["https://www.googleapis.com/auth/drive"];
  const auth = new google.auth.GoogleAuth({
    keyFile: keyfilepath,
    scopes: scopes,
  });
  const driveService = google.drive({
    version: "v3",
    auth,
  });

  let filemetadata = {
    name: nombretemp,
    parents: ["1dICEmRGrjZ7C67BvitgvMgwCpAiY9EFU"], //folder id de google drive
  };
  let media = {
    mimeType: mimetype, //se pasa el mimetype del archivo tal cual por lo tanto se puede subir cualquier tipo
    body: fs.createReadStream(tempFilePath),
  };

  let response = await driveService.files.create({
    resource: filemetadata,
    media: media,
    field: "id",
  });

  switch (response.status) {
    case 200:
      const { data } = response;
      const { id } = data;
      const link = "https://drive.google.com/uc?export=view&id=" + id;

      return link;
      break;

    default:
      throw new Error(`Error al subir el archivo `);
      break;
  }
};

const uploadfilesshort = async (tempFilePath, name, res = response) => {
  const nombrecortado = name.split(".");
  const extension = nombrecortado[nombrecortado.length - 1];
  const incluida = extensionesvalidasimg.includes(extension);
  if (!incluida) {
    throw new Error(`La extension no ${extension} está permitida `);
  }
  const nombretemp = uuidv4() + "." + extension;
  const keyfilepath = path.join(
    __dirname,
    "../Auth/rest-api-353417-32e9555ddd01.json"
  );
  const scopes = ["https://www.googleapis.com/auth/drive"];
  const auth = new google.auth.GoogleAuth({
    keyFile: keyfilepath,
    scopes: scopes,
  });
  const driveService = google.drive({
    version: "v3",
    auth,
  });
  let filemetadata = {
    name: nombretemp,
    parents: ["1dICEmRGrjZ7C67BvitgvMgwCpAiY9EFU"], //folder id de google drive
  };
  let media = {
    body: fs.createReadStream(tempFilePath),
  };

  let response = await driveService.files.create({
    resource: filemetadata,
    media: media,
    field: "id",
  });

  switch (response.status) {
    case 200:
      const { data } = response;
      const { id } = data;
      const link = "https://drive.google.com/uc?export=view&id=" + id;

      return link;
      break;

    default:
      throw new Error(`Error al subir el archivo `);
      break;
  }
};

const deletefiles = (pathdel) => {
  fs.readdir(pathdel, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(pathdel, file), (err) => {
        if (err) throw err;
      });
    }
  });
};

const actualizarimgen = async (req, res = response) => {
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
    res.status(400).json({ msg: "No files were uploaded." });
    return;
  }
  const { id, coleccion } = req.params;
  let modelo;
  switch (coleccion) {
    case "usuarios":
      modelo = await Usuario.findById(id);
      if (!modelo) {
        return res.status(400).json({
          msg: `no existe usuario con id: ${id}`,
        });
      }
      break;
    case "cursos":
      modelo = await Curso.findById(id);
      if (!modelo) {
        return res.status(400).json({
          msg: `no existe curso con id: ${id}`,
        });
      }
      break;

    default:
      return res.status(500).json({
        msg: "avisa al back que otras colecciones quieres",
      });
      break;
  }
  try {
    const link = await uploadfiles(req);
    modelo.img = link;
    await modelo.save();
    return res.json({
      modelo,
    });
  } catch (error) {
    return res.status(400).json({
      msg: `Algo fallo en la carga del archivo. Error : ${error.message}`,
    });
  }
};

const eliminarimg = async (req, res = response) => {
  const { id, coleccion } = req.params;
  let modelo;
  switch (coleccion) {
    case "usuarios":
      modelo = await Usuario.findById(id);
      if (!modelo) {
        return res.status(400).json({
          msg: `no existe usuario con id: ${id}`,
        });
      }
      break;
    case "cursos":
      modelo = await Curso.findById(id);
      if (!modelo) {
        return res.status(400).json({
          msg: `no existe curso con id: ${id}`,
        });
      }
      break;

    default:
      return res.status(500).json({
        msg: "Comunicate con el back para poder eliminar las img en otras colecciones",
      });
      break;
  }
  try {
    const response = await destroyfiles(modelo);
    if (response) {
      modelo.img = "";
      await modelo.save();
      return res.json({
        modelo,
      });
    }
  } catch (error) {
    return res.status(400).json({
      msg: `Algo fallo en la eliminación del archivo ${error.message}`,
    });
  }
};

const destroyfiles = async (modelo) => {
  const urlfile = modelo.img;
  const fileid = urlfile.split("id=")[1];
  const keyfilepath = path.join(
    __dirname,
    "../Auth/rest-api-353417-32e9555ddd01.json"
  );
  const scopes = ["https://www.googleapis.com/auth/drive"];
  const auth = new google.auth.GoogleAuth({
    keyFile: keyfilepath,
    scopes: scopes,
  });
  const driveService = google.drive({
    version: "v3",
    auth,
  });

  // Acquire an auth client, and bind it to all future calls
  const authClient = await auth.getClient();
  google.options({ auth: authClient });

  // Do the magic
  const response = await driveService.files.delete({
    // The ID of the file.

    fileId: fileid,
  });


  switch (response.status) {
    case 200:
      return true;
      break;

    case 204:
      return true;
    break;

    default:
      throw new Error(`Error al borrar el archivo `);
      break;
  }
};

const unzipfiles = async (pathzip, carpeta) => {
  try {
    const files = await decompress(fs.readFileSync(pathzip), carpeta);
    return files;
  } catch (error) {
    throw new Error(`Error al descomprimir el archivo ` + error.message);
  }
};

module.exports = {
  actualizarimgen,
  uploadfiles,
  uploadfilesshort,
  eliminarimg,
  unzipfiles,
  deletefiles,
};

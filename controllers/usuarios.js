const { response, request } = require("express");
const Facultad = require("../models/facultad");
const Usuario = require("../models/usuario");
const { crearcontraseña } = require("../helpers/create-randompass");
const { uploadfiles } = require("../controllers/uploads");
const { enviaremailuser } = require("../helpers/send-email");
const { ObjectId } = require("mongoose").Types;
const bcryptjs = require("bcryptjs");

const usuariosGet = async (req = request, res = response) => {
  const { limite = 5, desde = 0, id = "", nombre = "" } = req.query;

  const isMongoId = ObjectId.isValid(id);
  if (isMongoId) {
    const usuario = await Usuario.findById(id);
    if (usuario) {
      if (usuario.estado) {
        return res.json({
          items: usuario,
        });
      } else {
        return res.status(400).json({
          msg: "el usuario no existe",
        });
      }
    } else {
      return res.status(400).json({
        msg: "el usuario no existe",
      });
    }
  }

  if (nombre !== "") {
    const regEx = new RegExp(nombre, "i");
    const queryname = { nombre: regEx, estado: true };

    var [total, usuarios] = await Promise.all([
      Usuario.countDocuments(queryname),
      Usuario.find(queryname),
    ]);
    await changetobj(usuarios);
      return res.json({
        total,
        items: usuarios,
      });
  }

  const query = { estado: true };
  var [total, usuarios] = await Promise.all([
    Usuario.countDocuments(query),
    Usuario.find(query).skip(Number(desde)).limit(Number(limite)),
  ]);
  await changetobj(usuarios);
  res.json({
    total,
    items: usuarios,
  });
};

async function changetobj(usuarios) {
  for (const usuario of usuarios) {
    usuario.facultad = await Facultad.findById(usuario.facultad);
  }
}

const usuariosPost = async (req, res = response) => {
  const { nombre, correo, rol, facultad } = req.body;
  var password = "";
  const usuario = new Usuario({ nombre, correo, password, rol, facultad });

  //crear contraseña random, despues mandar mail
  password = crearcontraseña();

  // Encriptar la contraseña
  const salt = bcryptjs.genSaltSync();
  usuario.password = bcryptjs.hashSync(password, salt);
  //CARGAR USUARIO
  //*SI NO HAY IMAGEN DE PERFIL DEL USUARIO
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
    try {
      // Guardar en BD y envio de email con contraseña nueva
      await usuario.save();
      enviaremailuser(usuario.correo, password);
      return res.json({
        items: usuario,
      });
    } catch (error) {
      return res.status(400).json({
        msg: "Hubo un problema al crear usuario. Error: " + error.message,
      });
    }
    //*SI HAY UNA IMAGEN DE PERFIL DEL USUARIO
  } else {
    try {
      // Guardar en BD y envio de email con contraseña nueva
      const link = await uploadfiles(req);
      usuario.img = link;
      await usuario.save();
      enviaremailuser(usuario.correo, password);
      return res.json({
        items: usuario,
      });
    } catch (error) {
      return res.status(400).json({
        msg: "Hubo un problema al crear usuario. Error: " + error.message,
      });
    }
  }
};

const usuariosPut = async (req, res = response) => {
  const { id } = req.params;
  const { correo, rol, nombre, password } = req.body;
  const usuario = await Usuario.findById(id);

  //comprobación extra
  const usuarioemail = await Usuario.findOne({ correo });
  //validación para que pueda actualizar su email siempre y cuando no haya otro user que tenga el que va a poner
  if (usuarioemail && usuarioemail.id !== id) {
    return res.status(400).json({ msg: "El email ya se encuentra en uso" });
  }
  //nadie tiene permiso de editar al super_Admin EXCEPTO OTRO SUPER_aDMIN
  if (
    usuario.rol === "SUPER_ADMIN" &&
    (req.usuario.rol === "ADMIN_ROLE" || req.usuario.rol === "USER_ROLE")
  ) {
    return res.status(400).json({ msg: "No tienes permiso" });
  }
  //si el usuario loggeado es el "chalan" y quiere modificar a un admin o super no va ha poder
  if (
    req.usuario.rol === "USER_ROLE" &&
    (usuario.rol === "ADMIN_ROLE" || usuario.rol === "SUPER_ADMIN")
  ) {
    return res
      .status(401)
      .json({ msg: "No tienes permiso, solicitalo a tu jefe" });
  }
  //el "chalan" no puede modificar a otro user_role solo a sí mismo"
  if (req.usuario.rol === "USER_ROLE" && usuario.id !== req.usuario.id) {
    return res
      .status(400)
      .json({ msg: "No tienes permiso, solo te puedes modificar a ti mismo" });
  }
  // el "el chalan no puede pasarse a admin_role o superadmin"
  if (
    req.usuario.rol === "USER_ROLE" &&
    (rol === "ADMIN_ROLE" || rol === "SUPER_ADMIN")
  ) {
    return res
      .status(401)
      .json({ msg: "No tienes permiso, solicitalo a tu jefe" });
  }
  //SE VALIDA LA CONTRASEÑA DEL ADMINISTRADOR ACTUALMENTE LOGEADO
  const isValidcontraseña = bcryptjs.compareSync(
    password,
    req.usuario.password
  );
  if (!isValidcontraseña) {
    return res.status(400).json({
      msg: "La contraseña no es correcta",
    });
  }
  //comprueba que ya se haya cargado un archivo
  //*SI NO HAY ARCHIVO
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
    try {
      usuario.correo = correo;
      usuario.rol = rol;
      usuario.nombre = nombre;
      await usuario.save();
      return res.json({
        items: usuario,
      });
    } catch (error) {
      return res.status(400).json({
        msg: `Algo fallo al actualizar datos. Error: ${error.message}`,
      });
    }
    //*SI HAY UN ARCHIVO
  } else {
    try {
      // Guardar en BD y envio de email con contraseña nueva
      const link = await uploadfiles(req);
      usuario.img = link;
      usuario.correo = correo;
      usuario.rol = rol;
      usuario.nombre = nombre;
      await usuario.save();
      return res.json({
        items: usuario,
      });
    } catch (error) {
      return res.status(400).json({
        msg: `Algo fallo al actualizar datos. Error: ${error.message}`,
      });
    }
  }
};

//busca al usuario con el ID dado y actualiza el estado
const usuariosDelete = async (req, res = response) => {
  const { id } = req.params;
  const user = await Usuario.findById(id);
  if (!user) {
    return res.status(400).json({ msg: "El usuario no existe" });
  }
  if (user.rol === "SUPER_ADMIN") {
    return res.status(400).json({ msg: "No tienes permiso" });
  }
  await Usuario.findByIdAndUpdate(id, { estado: false });
  res.json({ msg: "El usuario ha sido eliminado" });
};

module.exports = {
  usuariosGet,
  usuariosPost,
  usuariosPut,
  usuariosDelete,
};

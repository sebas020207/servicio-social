const { response } = require("express");
const Curso = require("../../models/curso");
const { ObjectId } = require("mongoose").Types;

const publicursosget = async (req , res = response) => {
  const {
    id = "",
    nombre = "",
    limite = 5,
    desde = 0,
    facultad = "",
  } = req.query;
  //se comprueba que el id sea un id valido dentro de mongoDB
  const isMongoId = ObjectId.isValid(id);
  if (isMongoId) {
    //en caso de ser valido se intenta obtener el curso
    const curso = await Curso.findById(id);
    if (curso) {
      //si existe el curso
      if (curso.estado) {
        if (curso.publicado) {
          //si el curso esta activo y publicado
          return res.json({
            items: [curso],
          });
        } else {
          return res.status(401).json({
            msg: "el curso no existe", //por que está despublicado
          });
        }
      } else {
        return res.status(400).json({
          msg: "el curso no existe", //por que está desactivado
        });
      }
    } else {
      return res.status(400).json({
        msg: "el curso no existe", //por que si es un mongoID, pero no es precisamente el id de un curso
      });
    }
  }

  // busqueda por nombre
  //si el param "nombre" no esta vacio
  if (nombre !== "") {
    const regEx = new RegExp(nombre, "i");
    const query = { titulo: regEx, estado: true, publicado: true };
    //Se obtiene el curso con el query definido arriba
    const coursename = await Curso.find(query);
    if (coursename) {
      return res.json({
        items: [coursename],
      });
    }
  }

  //regresa todos los cursos con las mismas condiciones de arriba
  // si la facultad no esta vacia entonces todos los activos de acuerdo a la facultad especificada
  // si la facultad esta vacia entonces todos los activos y publicados solamente
  const query =
    facultad !== ""
      ? { estado: true, facultad: facultad, publicado: true }
      : { estado: true, publicado: true };
  //cursos es la lista de cursos
  var [cursos] = await Promise.all([
    Curso.find(query).skip(Number(desde)).limit(Number(limite)),
  ]);

  res.json({
    items: cursos,
  });
};

module.exports = { publicursosget };

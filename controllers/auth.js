const { response, request } = require("express");
const bcryptjs = require("bcryptjs");
const Usuario = require("../models/usuario");
const { generarJWT, generarrefreshJWT } = require("../helpers/generar-jwt");
const { validandcreatenewtoken } = require("../middlewares/validarjwt");
const Facultad = require("../models/facultad");
const { stringify } = require("querystring");
const fetch = require("node-fetch");
const { enviaremail } = require("../helpers/send-email");

const login = async (req, res = response) => {
  const { correo, password } = req.body;
  try {
    //que el usuario existe
    const usuario = await Usuario.findOne({ correo });
    if (!usuario) {
      return res.status(400).json({
        msg: "Email / password son incorrectos",
      });
    }

    //si el usuario esta activo
    if (!usuario.estado) {
      return res.status(400).json({
        msg: "Usuario está inactivo",
      });
    }

    //que la contraseña sea la misma
    const isValidcontraseña = bcryptjs.compareSync(password, usuario.password);
    if (!isValidcontraseña) {
      return res.status(400).json({
        msg: "Password incorrecto",
      });
    }

    //generar el JWT
    const token = await generarJWT(usuario.id);
    const refresh = await generarrefreshJWT(usuario.id);
    res.json({
      usuario,
      token,
      refresh,
    });
  } catch (error) {
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

const refresh = async (req = request, res = response) => {
  await validandcreatenewtoken(req, res);
};

const my_info = async (req, res = response) => {
  const { facultad } = await Facultad.findById(req.usuario.facultad);
  res.json({
    id: req.usuario.id,
    nombre: req.usuario.nombre,
    correo: req.usuario.correo,
    estado: req.usuario.estado,
    img: req.usuario.img,
    facultad: facultad,
    rol: req.usuario.rol,
  });
};

const forgot_password = async (req = request, res = response) => {
  const { correo } = req.body;
  const message =
    "Revisa tu correo e ingresa al link para cambiar tu contraseña";
  let verificationlink;
  //obtenemos el usuario con el correo especificado en el body
  const usuario = await Usuario.findOne({ correo });
  //creamos un nuevo token con el id del usuario expira en 4h
  const token = await generarJWT(usuario.id);
  //creamos el link de verificacion
  verificationlink = `http://localhost:3000/restore-password/${token}/`;

  try {
    //enviamos el mail
    enviaremail(correo, verificationlink);
  } catch (error) {
    return res.status(400).json({
      msg: error.message,
    });
  }
  res.json({
    msg: message,
  });
};

const createnewpassword = async (req = request, res = response) => {
  const { password } = req.body;
  //obtenemos el modelo del usuario
  try {
    const usuario = await Usuario.findById(req.usuario.id);
    //encriptamos la contraseña y la asignamos
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password, salt);
    //guardamos los cambios
    await usuario.save();
    return res.json({
      msg: "Se ha cambiado la contraseña exitosamente",
    });
  } catch (error) {
    return res.status(400).json({
      msg: "Error: "+ error.message
    }) ;
  }
};

const captcha = async (req, res = response) => {
  if (!req.body.captcha)
    return res
      .status(400)
      .json({ success: false, msg: "Por favor completa el captcha" });

  const secretKey = "6Lcm6dIgAAAAAABEGi3-le_ABIYnfo5xMDrUPicl";

  const query = stringify({
    secret: secretKey,
    response: req.body.captcha,
    remoteip: req.connection.remoteAddress,
  });
  const verifyURL = `https://google.com/recaptcha/api/siteverify?${query}`;

  const body = await fetch(verifyURL).then((res) => res.json());

  if (body.success !== undefined && !body.success)
    return res
      .status(400)
      .json({ success: false, msg: "La verificación fallo" });

  return res.json({ success: true, msg: "Correcto" });
};

const changepassword = async (req = request, res = response) => {
  const { current_password, new_password } = req.body;
  //que la contraseña sea la misma "del usuario loggeado"
  const isValidcontraseña = bcryptjs.compareSync(
    current_password,
    req.usuario.password
  );
  if (!isValidcontraseña) {
    return res.status(400).json({
      msg: "Password incorrecto",
    });
  }
  try {
    //obtenemos el modelo del usuario actual
    const usuario = await Usuario.findById(req.usuario.id);
    //encriptamos la contraseña y la asignamos
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(new_password, salt);
    //guardamos los cambios
    await usuario.save();
    res.json({
      msg: "Se ha cambiado la contraseña exitosamente",
    });
  } catch (error) {
    res.status(400).json({
      msg: "Error: " + error.message,
    });
  }
};
/*   const googlesignin = async (req, res = response) => {
      const { id_token } = req.body;
      try{
        const {nombre,correo,img} = await googleverify(id_token);
        let usuario = await Usuario.findOne({correo});
        if(!usuario){
              const data = {
                nombre: nombre,
                correo: correo,
                img: img,
                password: ":D",
                rol: "ADMIN_ROLE",
                facultad: "62abe01062da882e0260f3d2",  //object id de facultad default
                google: true,
              };
            usuario = new Usuario(data);
            await usuario.save();
        }
        if(!usuario.estado){
            return resp.json({
              msg: "el usuario ya no existe",
            });
        }

        const token = await generarJWT(usuario.id);

        res.json({
        usuario,
        token
        });
      }catch(error){
        resp.status(400).json({
            error
        })

      }
      
    } */

module.exports = {
  login,
  refresh,
  my_info,
  forgot_password,
  captcha,
  createnewpassword,
  changepassword,
};

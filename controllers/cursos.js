const { response, request } = require("express");
const Facultad = require("../models/facultad");
const Curso = require("../models/curso");
const {
  uploadfiles,
  unzipfiles,
  uploadfilesshort,
  deletefiles,
} = require("../controllers/uploads");
const { ObjectId } = require("mongoose").Types;
var xlsx = require("xlsx");
const path = require("path");
const { getJsDateFromExcel } = require("excel-date-to-js");

const cursosGet = async (req = request, res = response) => {
  const {
    id = "",
    nombre = "",
    limite = 5,
    desde = 0,
    fecha_inicio,
  } = req.query;
  const fac = req.query.facultad;
  //se obtiene el nombre de la facultad del usuario que esta actualmente loggeado
  const { facultad } = await Facultad.findById(req.usuario.facultad);
  //se comprueba que el id sea un id valido dentro de mongoDB
  const isMongoId = ObjectId.isValid(id);
  if (isMongoId) {
    //en caso de ser valido se intenta obtener el curso
    const curso = await Curso.findById(id);
    if (curso) {
      //si existe el curso
      if (curso.estado) {
        //si el curso esta activo
        if (
          // solo se va ha mostrar si de la misma facultad que el user role
          // el resto de roles pueden ver el curso sin importar la facultad
          (req.usuario.rol === "USER_ROLE" &&
            curso.facultad === req.usuario.facultad) ||
          req.usuario.rol === "ADMIN_ROLE" ||
          req.usuario.rol === "SUPER_ADMIN"
        ) {
          return res.json({
            items: [curso],
          });
        } else {
          return res.status(401).json({
            msg: "Este curso no esta disponible para ti", //por que eres user_role y ese curso no es de tu fac
          });
        }
      } else {
        return res.status(400).json({
          msg: "el curso no existe", //por que está desactivado
        });
      }
    } else {
      return res.status(400).json({
        msg: "el curso no existe", //por que si es un mongoID, pero no es precisamente el id de un curso
      });
    }
  }

  // busqueda por nombre
  //si el param "nombre" no está vacio
  if (nombre) {
    const regEx = new RegExp(nombre, "i"); //expresión regular permite buscar cursos sin importar entrada MAYUS/min
    //si es admin o superadmin va a poder ver todos los cursos activos, si es user_ solo va a regresar los de su facultad
    const query =
      req.usuario.rol === "ADMIN_ROLE" || req.usuario.rol === "SUPER_ADMIN"
        ? { titulo: regEx, estado: true }
        : { estado: true, facultad: req.usuario.facultad, titulo: regEx };
    //Se obtiene el curso con el query definido arriba
    const coursename = await Curso.find(query)
      .skip(Number(desde))
      .limit(Number(limite));
    const total = await Curso.countDocuments(query);
    if (coursename) {
      return res.json({
        total,
        items: coursename,
      });
    }
  }

  //BUSQUEDA POR ID DE FACULTAD
  //verifica que el id de facultad sea un id de mongo
  const isMongoIdfac = ObjectId.isValid(fac);
  if (isMongoIdfac) {
    const existeFac = await Facultad.findById(fac);
    //verificar que exista la facultad
    if (!existeFac) {
      return res.status(400).json({
        msg: "la facultad con id especificado no existe",
      });
    }
    //comprobar si es el user_role no pueda ver estos cursos a menos que sea su fac
    if (
      (req.usuario.rol === "USER_ROLE" && existeFac.facultad === facultad) ||
      req.usuario.rol === "ADMIN_ROLE" ||
      req.usuario.rol === "SUPER_ADMIN"
    ) {
      //todos los cursos de la facultad especificada que esten activos
      var [total, cursos] = await Promise.all([
        Curso.countDocuments({
          facultad: existeFac.id,
          estado: true,
        }),
        Curso.find({ facultad: existeFac.id, estado: true })
          .skip(Number(desde))
          .limit(Number(limite)),
      ]);
      return res.json({
        total,
        items: cursos,
      });
    } else {
      return res.status(403).json({
        msg: "No tienes acceso a estos cursos",
      });
    }
  }
  //BUSQUEDA POR FECHA DE INICIO
  if (fecha_inicio) {
    try {
      const fecha = new Date(fecha_inicio);
      const query =
        req.usuario.rol === "USER_ROLE"
          ? {
              fecha_inicio: fecha,
              estado: true,
              facultad: req.usuario.facultad,
            }
          : {
              fecha_inicio: fecha,
              estado: true,
            };
      const cursos = await Curso.find(query)
        .skip(Number(desde))
        .limit(Number(limite));
      if (cursos.length > 0) {
        const total = await Curso.countDocuments(query);
        return res.json({
          total,
          items: cursos,
        });
      } else {
        return res.json({
          total: 0,
          items: [],
        });
      }
    } catch (error) {
      return res.status(400).json({
        msg: "Verifica la fecha de inicio. Error: " + error.message,
      });
    }
  }

  //regresa todos los cursos
  const query =
    req.usuario.rol === "ADMIN_ROLE" || req.usuario.rol === "SUPER_ADMIN"
      ? { estado: true }
      : { estado: true, facultad: req.usuario.facultad };
  //cursos es la lista de cursos
  var [total, cursos] = await Promise.all([
    Curso.countDocuments(query),
    Curso.find(query).skip(Number(desde)).limit(Number(limite)),
  ]);

  res.json({
    total,
    items: cursos,
  });
};

const cursosPost = async (req, res = response) => {
  const course = req.body;
  let data;
  data = await getvalidatedata(req, course);
  const curso = new Curso(data);
  //carga de archivo opcional
  //*SI NO HAY ARCHIVO
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
    try {
      // Guardar en BD
      await curso.save();
      return res.json({
        items: curso,
      });
    } catch (error) {
      return res.status(400).json({
        msg: `Algo fallo en la carga del curso. Error : ` + error.message,
      });
    }
    //*SI HAY ARCHIVO
  } else {
    try {
      // Guardar en BD
      const link = await uploadfiles(req);
      curso.img = link;
      await curso.save();
      return res.json({
        items: curso,
      });
    } catch (error) {
      return res.status(400).json({
        msg: `Algo fallo en la carga del curso. Error : ` + error.message,
      });
    }
  }
};

const cursosPut = async (req, res = response) => {
  const { id } = req.params;
  const { registro_institucional, facultad, estado, ...resto } = req.body;
  const curso = await Curso.findById(id);
  let facultadfinal;
  if (req.usuario.rol === "USER_ROLE") {
    //el user role no podrá actualizar la la facultad siempre será la misma
    facultadfinal = curso.facultad;
  } else {
    facultadfinal = facultad;
  }

  const courseregistry = await Curso.findOne({ registro_institucional });
  //validación para que pueda actualizar su registro_institucional siempre y cuando no haya otro curso que tenga el que va a poner
  if (courseregistry && courseregistry.id !== id) {
    return res.status(400).json({ msg: "El registro ya se encuentra en uso" });
  }
  //carga de archivo opcional
  //*SI NO HAY ARCHIVO
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
    try {
      // Guardar en BD
      curso.registro_institucional = registro_institucional;
      curso.facultad = facultadfinal;
      await curso.save();
      const courseupdated = await Curso.findByIdAndUpdate(id, resto);
      return res.json({
        items: courseupdated,
      });
    } catch (error) {
      return res.status(400).json({
        msg: `Algo fallo en la carga del curso. Error : ` + error.message,
      });
    }
    //**SI HAY ARCHIVO */
  } else {
    try {
      // Guardar en BD
      const link = await uploadfiles(req);
      curso.img = link;
      curso.registro_institucional = registro_institucional;
      curso.facultad = facultadfinal;
      await curso.save();
      await Curso.findByIdAndUpdate(id, resto);
      return res.json({
        items: curso,
      });
    } catch (error) {
      return res.status(400).json({
        msg: `Algo fallo en la carga del curso. Error : ` + error.message,
      });
    }
  }
};

const cursosPatch = (req, res = response) => {
  res.json({
    msg: "patch API - usuariosPatch",
  });
};

const cursosDelete = async (req, res = response) => {
  const { id } = req.params;

  // Fisicamente lo borramos
  // const usuario = await Usuario.findByIdAndDelete( id );

  await Curso.findByIdAndUpdate(id, { estado: false });

  res.json({ msg: "El curso ha sido borrado" });
};

const cursosPublish = async (req, res = response) => {
  const { id } = req.params;
  const { publicado } = req.body;
  const curso = await Curso.findByIdAndUpdate(id, { publicado: publicado });
  const mensaje = curso.publicado
    ? "El curso ha sido publicado"
    : " El curso ha sido despublicado";
  res.json({
    mensaje,
  });
};

const getvalidatedata = async (req, course) => {
  const {
    titulo,
    registro_institucional,
    fecha_inicio,
    fecha_final,
    contacto,
    telefono,
    extension = 0,
    precio = 0,
    correo,
    extradata = "",
    facultad,
    publicado,
    img = "",
  } = course;
  var data;
  if (req.usuario.rol === "USER_ROLE") {
    //el user_role solo podrá crear cursos dentro de su facultad.
    data = {
      facultad: req.usuario.facultad,
      titulo: titulo,
      registro_institucional: registro_institucional,
      fecha_inicio: fecha_inicio,
      fecha_final: fecha_final,
      contacto: contacto,
      telefono: telefono,
      extension: extension,
      precio: precio,
      correo: correo,
      extradata: extradata,
      publicado:
        publicado === "" ||
        publicado === undefined ||
        publicado === null ||
        publicado == "false"
          ? false
          : true,
      img: img,
    };
  } else {
    data = {
      facultad: facultad,
      titulo: titulo,
      registro_institucional: registro_institucional,
      fecha_inicio: fecha_inicio,
      fecha_final: fecha_final,
      contacto: contacto,
      telefono: telefono,
      extension: extension,
      precio: precio,
      correo: correo,
      extradata: extradata,
      publicado:
        publicado === "" ||
        publicado === undefined ||
        publicado === null ||
        publicado === "false"
          ? false
          : true,
      img: img,
    };
  }
  return data;
};

const postfromexcel = async (req, res = response) => {
  var io = req.app.get("socketio");
  let msg;
  const sockid = req.body.socketid;
  var socket = io.sockets.sockets.get(sockid); // Find socket by id
  if (socket) {
    socket.emit("enviar-mensaje", "Comenzando a cargar excel :D.");
  } else {
    return res.status(400).json({
      msg: "No existe socket , verificalo",
    });
  }
  var data,
    files = [];
  //obtener path temporal
  const { tempFilePath } = req.files.archivo;
  //comprobar que si es que se cargo o no el archivo zip de imagenes
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.zip) {
    //no hat archivo zip
    //obtener libro
    const workbook = xlsx.readFile(tempFilePath);
    //obtener las hojas del libro
    const workbooksheets = workbook.SheetNames;
    //tomamos la primera hoja
    const sheet = workbooksheets[0];
    //la pasamos a json
    const dataexcel = xlsx.utils.sheet_to_json(workbook.Sheets[sheet]);
    for (const course of dataexcel) {
      try {
        data = await getvalidatedata(req, course); //se valida la info con la informacion del usuario actual
        //** Se convierten las fechas de excel a Date de JS  */
        data.fecha_inicio = getJsDateFromExcel(data.fecha_inicio);
        data.fecha_final = getJsDateFromExcel(data.fecha_final);
        var curso = new Curso(data); //creamos el nuevo curso
        socket.emit(
          "progress",
          `${dataexcel.indexOf(course) + 1} de ${dataexcel.length}`
        );

        await curso.save();
        socket.emit(
          "enviar-mensaje",
          "Se cargo correctamente el curso" + curso
        );
      } catch (error) {
        if (error.message.indexOf("duplicate key error") !== -1) {
          msg = `PRECAUCIÓN: El curso ${curso.titulo} esta duplicado. Linea: ${
            dataexcel.indexOf(course) + 2
          }`;
        } else if (error.message.indexOf("validation failed") !== -1) {
          msg =
            `ADVERTENCIA: En la línea: ${
              dataexcel.indexOf(course) + 2
            } Verifique: ` + error.message;
        } else if (error.message.indexOf("wrong input format") !== -1) {
          msg = `ADVERTENCIA: En la línea: ${
            dataexcel.indexOf(course) + 2
          } Verifique: las fechas `;
        } else {
          msg = error.message;
        }
        socket.emit("enviar-mensaje", "Error: " + msg);
      }
    }
  } else {
    // si hay archivo zip con imagenes
    // tiene que ser obligatoriamente zip
    const extensionesvalidas = ["zip"];
    const { name } = req.files.zip;
    const nombrecortado = name.split(".");
    const extension = nombrecortado[nombrecortado.length - 1];
    const incluida = extensionesvalidas.includes(extension);
    if (!incluida) {
      res
        .status(400)
        .json({
          msg:
            "Solo extensiones permitidas: " +
            extensionesvalidas +
            ". Para el archivo comprimido",
        });
      return;
    }
    //se obtiene el path temporal del zip
    const pathzip = req.files.zip.tempFilePath;
    try {
      //descomprimirmos el archivo pasando el nombre del path y la carpeta destino
      //files es un array con los metadatos de los archivos descomprimidos
      files = await unzipfiles(pathzip, "assets");
      //console.log(files);
    } catch (error) {
      return res.status(400).json({
        msg: "Error : " + error.message,
      });
    }
    //obtener libro
    const workbook = xlsx.readFile(tempFilePath);
    //obtener las hojas del libro
    const workbooksheets = workbook.SheetNames;
    //tomamos la primera hora
    const sheet = workbooksheets[0];
    //la pasamos a json
    const dataexcel = xlsx.utils.sheet_to_json(workbook.Sheets[sheet]);

    for (const course of dataexcel) {
      try {
        data = await getvalidatedata(req, course); //se valida la info con la informacion del usuario actual
        //** Se convierten las fechas de excel a Date de JS  */
        data.fecha_inicio = getJsDateFromExcel(data.fecha_inicio);
        data.fecha_final = getJsDateFromExcel(data.fecha_final);
        var curso = new Curso(data); //creamos el nuevo curso
        socket.emit(
          "progress",
          `${dataexcel.indexOf(course) + 1} de ${dataexcel.length}`
        );

        // Guardar en BD
        // comprobamos que dentro exista una imagen con el registro institucional y algun formato valido
        const file = files.filter(
          (e) =>
            e.path === dataexcel.indexOf(course) + 2 + ".jpg" ||
            e.path === dataexcel.indexOf(course) + 2 + ".png" ||
            e.path === dataexcel.indexOf(course) + 2 + ".jpeg"
        );
        if (file.length > 0) {
          const pathimg = path.join("./assets/" + file[0].path);
          const link = await uploadfilesshort(pathimg, file[0].path);
          curso.img = link;
        } else {
          socket.emit(
            "enviar-mensaje",
            `\nADVERTENCIA: En acto academico: ${curso.titulo} no se cargo foto `
          );
        }

        await curso.save();
        socket.emit(
          "enviar-mensaje",
          "Se cargo correctamente el curso" + curso
        );
      } catch (error) {
        if (error.message.indexOf("duplicate key error") !== -1) {
          msg = `PRECAUCIÓN: El curso ${curso.titulo} esta duplicado.`;
        } else if (error.message.indexOf("validation failed") !== -1) {
          msg =
            `ADVERTENCIA: En la línea: ${
              dataexcel.indexOf(course) + 2
            } Verifique: ` + error.message;
        } else if (error.message.indexOf("wrong input format") !== -1) {
          msg = `ADVERTENCIA: En la línea: ${
            dataexcel.indexOf(course) + 2
          } Verifique: las fechas `;
        } else {
          msg = error.message;
        }
        socket.emit("enviar-mensaje", "Error: " + msg);
      }
    }
    try {
      deletefiles("assets"); //eliminamos los archivos de la carpeta assets ya que no serán necesarios almacenarlos  2 veces (google drive y aqui)
    } catch (error) {
      return res.status(400).json({
        msg: "Error : " + error.message,
      });
    }
  }
  return res.json({
    msg: "ok",
  })
};

module.exports = {
  cursosGet,
  cursosPost,
  cursosPut,
  cursosPatch,
  cursosDelete,
  cursosPublish,
  postfromexcel,
};

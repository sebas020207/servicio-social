

const socketControll = (socket) => {

  console.log("Cliente conectado", socket.id);

  socket.on("disconnect", () => {
    console.log("Cliente desconectado", socket.id);
  });
};

module.exports = {
  socketControll,
};

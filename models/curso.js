const { Schema, model } = require("mongoose");

const cursoSchema = Schema({
  facultad: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  estado: {
    type: Boolean,
    default: true,
  },
  publicado: {
    type: Boolean,
    default: false,
  },
  titulo: {
    type: String,
    required: [true, "El titulo es obligatorio"],
  },
  registro_institucional: {
    type: String,
    required: [true, "El registro institucional es obligatorio"],
    unique: true,
  },
  fecha_inicio: {
    type: Date,
    required: [true, "La fecha de inicio es obligatoria"],
  },
  fecha_final: {
    type: Date,
    required: [true, "La fecha de  final es obligatoria"],
  },
  contacto: {
    type: String,
    required: [true, "El contacto es obligatorio"],
  },
  telefono: {
    type: Number,
    required: [true, "El telefono es obligatorio"],
  },
  extension: {
    type: Number,
    required: false,
  },
  precio: {
    type: Number,
    required: false,
  },
  correo: {
    type: String,
    required: [true, "El correo es obligatorio"],
  },
  extradata: {
    type: String,
    required: false,
  },
  img: {
    type: String,
    required: false,
  },
});

module.exports = model("curso", cursoSchema);

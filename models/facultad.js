const { Schema, model } = require("mongoose");

const facultadSchema = Schema({
  facultad: {
    type: String,
    required: [true, "La facultad es obligatoria"],
  },
  siglas :{
    type: String,
    required : [true, "Las siglas son obligatorias"]
  },
  link:{
    type:String,
  }
});

module.exports = model("facultad", facultadSchema);

const express = require("express");
const cors = require("cors");
const mongoSanitize = require("express-mongo-sanitize");
const { dbConnection } = require("../database/config");
const fileUpload = require("express-fileupload");
const bodyParser = require("body-parser");
const { socketControll } = require("../controllers/socketcontrol");

class Server {
  constructor() {
    
    this.app = express();
    this.port = process.env.PORT;
    this.server = require("http").createServer(this.app);
    this.io = require("socket.io")(this.server);
    this.usuariosPath = "/api/usuarios";
    this.authPath = "/api/auth";
    this.uploadsPath = "/api/uploads";
    this.facultadesPath = "/api/facultades";
    this.cursosPath = "/api/cursos";
    this.publicursosPath = "/api/public/cursos";

    // Conectar a base de datos
    this.conectarDB();

    // Middlewares
    this.middlewares();

    // Rutas de mi aplicación
    this.routes();

    //implementacion de sockets
    this.sockets();

    this.app.set("socketio", this.io);

  }


  async conectarDB() {
    await dbConnection();
  }

  middlewares() {
    // CORS
    this.app.use(cors());

    // Lectura y parseo del body
    this.app.use(express.json());

    // Directorio Público
    this.app.use(express.static("public"));

    //carga de archivos
    this.app.use(
      fileUpload({
        useTempFiles: true,
        tempFileDir: "/tmp/",
        createParentPath: true,
      })
    );

    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.app.use(
      mongoSanitize({
        onSanitize: ({ req, key }) => {
          console.warn(`This request[${key}] is sanitized`);
        },
      })
    );
  }

  routes() {
    this.app.use(this.usuariosPath, require("../routes/usuarios"));
    this.app.use(this.authPath, require("../routes/auth"));
    this.app.use(this.uploadsPath, require("../routes/uploads"));
    this.app.use(this.facultadesPath, require("../routes/facultades"));
    this.app.use(this.cursosPath, require("../routes/cursos"));
    this.app.use(
      this.publicursosPath,
      require("../routes/public_routes/cursos")
    );
  }

  sockets(){
    this.io.on('connection', socketControll);
  }

  listen() {
    this.server.listen(this.port, () => {
      console.log("Servidor corriendo en puerto", this.port);
    });
  }
}

module.exports = Server;
